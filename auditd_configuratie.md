## Configuratie auditd

### Configuratie van auditd op Debian (loggen van procesuitvoering)
Om beveiligingsincidenten effectief te kunnen detecteren en erop te reageren, is het belangrijk om goede logging gegevens te hebben voor beveiligingsrelevante gebeurtenissen die op een systeem plaatsvinden. 
Dit soort logs kan bestaan uit het uitvoeren van processen, bestandstoegang, het aanmaken van gebruikers, enz.

Met name logboeken over de uitvoering van processen zijn ongelooflijk waardevol. 
Logfiles, die door auditd worden gegenereerd, kunnen helpen een spoor te creëren van alle acties die op het systeem hebben plaatsgevonden. 
Dit kan dan helpen bij het bepalen van tijdlijnen, en het bepalen van de juiste volgende stappen voor onderzoek.

### Het auditsysteem op hoog niveau
Een belangrijk mechanisme om dit soort logging details op een Linux systeem te krijgen is het Audit subsysteem, onderdeel van de Linux kernel. 
Het Audit subsysteem kan worden gebruikt om kernel gebeurtenissen, zoals specifieke systeem aanroepen, en gebruikers gebeurtenissen te loggen. 
Het bestaat uit een kernel subsysteem dat de logging implementeert, en een audit daemon die deze logs verzamelt en ze naar een set van log entries schrijft. 
Er is ook een Audit dispatcher daemon (audispd) die een real-time mechanisme biedt voor de interactie tussen analytische programma's.

Tenslotte is er een set van tools die kunnen worden gebruikt om het genereren van gebeurtenissen te configureren. 
Deze bevatten auditctl, waarmee instellingen en parameters kunnen worden geconfigureerd van hoe evenementen worden gegenereerd, aureport, waarmee een rapport kan worden gegenereerd van gebeurtenissen die zijn verzameld door de audit daemon, en ausearch, waarmee logs kunnen worden doorzocht.

### Installeren van auditd
Om auditd te implementeren op Debian, moet je er eerst voor zorgen dat de audit daemon, en het audit dispatching raamwerk zijn geïnstalleerd op het systeem:

 apt-get install auditd audispd-plugins

=== Uitrollen van een initiële configuratie
Zodra deze zijn geinstalleerd, kan je een eerste regel set implementeren door het bestand /etc/audit/rules.d/audit.rules te bewerken. 
Het volgende is een verstandige regelset die je een paar sleutelelementen geeft. 
Het is een configuratie die met succes heeft gedraaid, gebaseerd op de https://gist.github.com/Neo23x0/9fe88c0c5979e017a389b90fd19ddfee[Linux Auditd] best practice configuratie van Florian Roth, en de https://github.com/alphagov/puppet-auditd/pull/1[GDS configuratie] van de UK Government Digital Service. 
Met dank aan hen voor het ontwikkelen en delen van een aantal bruikbare auditd startpunt configuraties.

## Auditd configuratie
## Configuratie hint:
:
## -w watch file system object, -p sets [r|w|x|a]
## -a regel toevoegen: exit, bij syscall exit

## Verwijder alle bestaande regels
-D

## Het hebben van een grote buffer zorgt ervoor dat we het laten vallen van logs voorkomen
-b 8192

## Faalmodus
## Mogelijke waarden zijn 0 (stil), 1 (printk, druk een foutmelding af),
## en 2 (paniek, het systeem stoppen).
-f 1

## Audit de audit logs, en uitvoering van auditd rapportage tools

-w /var/log/audit/ -k auditlog
-w /etc/audit/ -p wa -k auditconfig
-w /etc/libaudit.conf -p wa -k auditconfig
-w /etc/audisp/ -p wa -k audispconfig
-w /sbin/auditctl -p x -k audittools
-w /sbin/auditd -p x -k audittools
-a always,exit -F dir=/var/log/audit/ -F perm=r -F auid>=1000 -F auid!=unset -k audittools
-a always,exit -F path=/usr/sbin/ausearch -F perm=x -k audittools
-a always,exit -F path=/usr/sbin/aureport -F perm=x -k audittools
-a always,exit -F path=/usr/sbin/aulast -F perm=x -k audittools
-a always,exit -F path=/usr/sbin/aulastlogin -F perm=x -k audittools
-a always,exit -F path=/usr/sbin/auvirt -F perm=x -k audittools

## Log alle procesuitvoering: hier standaard uitgeschakeld vanwege belasting
# -a exit,always -S execve -k cmd
## Log alle procesuitvoer door root
-a exit,always -F arch=b64 -F euid=0 -S execve -k rootcmd
-a exit,always -F arch=b32 -F euid=0 -S execve -k rootcmd

## Identificeer het aanmaken van bestandssysteem nodes en bestandssysteem mounts
-a exit,always -F arch=b32 -S mknod -S mknodat -k specialfiles
-a exit,always -F arch=b64 -S mknod -S mknodat -k specialfiles
-a exit,always -F arch=b32 -S mount -S umount -S umount2 -k mount
-a exit,always -F arch=b64 -S mount -S umount2 -k mount

## Klok en tijdzone veranderingen
-a exit,always -F arch=b32 -S adjtimex -S settimeofday -S clock_settime -k time
-a exit,always -F arch=b64 -S adjtimex -S settimeofday -S clock_settime -k time
-w /etc/localtime -p wa -k localtime

## Updates naar cron
-w /etc/cron.allow -p wa -k cron
-w /etc/cron.deny -p wa -k cron
-w /etc/cron.d/ -p wa -k cron
-w /etc/cron.daily/ -p wa -k cron
-w /etc/cron.hourly/ -p wa -k cron
-w /etc/cron.monthly/ -p wa -k cron
-w /etc/cron.weekly/ -p wa -k cron
-w /etc/crontab -p wa -k cron
-w /var/spool/cron/crontabs/ -k cron

## Credential/user/group/login changes
-w /etc/group -p wa -k etcgroup
-w /etc/passwd -p wa -k etcpasswd
-w /etc/gshadow -k etcgroup
-w /etc/shadow -k etcpasswd
-w /etc/security/opasswd -k opasswd
-w /usr/bin/passwd -p x -k passwd_modification
-w /usr/sbin/groupadd -p x -k group_modification
-w /usr/sbin/groupmod -p x -k group_modification
-w /usr/sbin/addgroup -p x -k group_modification
-w /usr/sbin/useradd -p x -k user_modification
-w /usr/sbin/usermod -p x -k user_modification
-w /usr/sbin/adduser -p x -k user_modification
-w /etc/login.defs -p wa -k login
-w /etc/securetty -p wa -k login
w /var/log/faillog -p wa -k login
-w /var/log/lastlog -p wa -k login
-w /var/log/tallylog -p wa -k login

## Wijzigingen in netwerkconfiguraties
-w /etc/hosts -p wa -k hosts
-w /etc/network/ -p wa -k network

## opstartscripts voor het systeem
-w /etc/inittab -p wa -k init
-w /etc/init.d/ -p wa -k init
-w /etc/init/ -p wa -k init

## library zoekpaden
-w /etc/ld.so.conf -p wa -k libpath

## kernel parameters
-w /etc/sysctl.conf -p wa -k sysctl

## modprobe configuratie
-w /etc/modprobe.conf -p wa -k modprobe

## pam configuratie
-w /etc/pam.d/ -p wa -k pam
-w /etc/security/limits.conf -p wa -k pam
-w /etc/security/pam_env.conf -p wa -k pam
-w /etc/security/namespace.conf -p wa -k pam
-w /etc/security/namespace.init -p wa -k pam


## systeem configuratie veranderingen
-w /etc/ssh/sshd_config -k sshd
-a exit,always -F arch=b32 -S sethostname -k hostname
-a exit,always -F arch=b64 -S sethostname -k hostname
-w /etc/issue -p wa -k etcissue
-w /etc/issue.net -p wa -k etcissue


## Leg alle mislukte toegang vast op kritieke elementen
-a exit,always -F arch=b64 -S open -F dir=/etc -F success=0 -k unauthedfileacess
-a exit,always -F arch=b64 -S open -F dir=/bin -F success=0 -k unauthedfileacess
-a exit,always -F arch=b64 -S open -F dir=/sbin -F success=0 -k unauthedfileacess
-a exit,always -F arch=b64 -S open -F dir=/usr/bin -F success=0 -k unauthedfileacess
-a exit,always -F arch=b64 -S open -F dir=/usr/sbin -F success=0 -k unauthedfileacess
-a exit,always -F arch=b64 -S open -F dir=/var -F success=0 -k unauthedfileacess
-a exit,always -F arch=b64 -S open -F dir=/home -F success=0 -k unauthedfileacess
-a exit,always -F arch=b64 -S open -F dir=/srv -F success=0 -k unauthedfileacess

## Monitor voor gebruik van proces ID verandering (wisselen van account) toepassingen
-w /bin/su -p x -k priv_esc
-w /usr/bin/sudo -p x -k priv_esc
-w /etc/sudoers -p rw -k priv_esc

## Monitor het gebruik van commando's om de energietoestand te veranderen
-w /sbin/shutdown -p x -k power
-w /sbin/poweroff -p x -k power
-w /sbin/reboot -p x -k power
-w /sbin/halt -p x -k power

## Sta geen configuratiewijzigingen toe
## -e 0 schakelt uit, -e 1 schakelt in, -e 2 vergrendelt configuratie tot reboot
-e 1

Als de configuratie is opgeslagen, herstart dan auditd met /etc/init.d/auditd restart om de configuratie opnieuw te laden.

=== Toegang tot logs
Er zijn een aantal manieren om logs te verkrijgen en er actie op te ondernemen. 
Ten eerste worden logs op de meeste systemen geschreven naar /var/log/audit.log. Deze logs zien er als volgt uit:

type=SYSCALL msg=audit(1520912790. 481:50015): arch=c000003e syscall=59 success=yes exit=0 a0=132ed88 a1=132ef48 a2=12de208 a3=5d3 items=2 ppid=18673 pid=18677 auid=1000 uid=0 gid=0 euid=0 suid=0 fsuid=0 egid=0 sgid=0 fsgid=0 tty=pts0 ses=1669 comm="tail" exe="/usr/bin/tail" key="rootcmd" +
type=EXECVE msg=audit(1520912790.481:50015): argc=3 a0="tail" a1="-f" a2="audit.log" +
type=CWD msg=audit(1520912790.481:50015): cwd="/var/log/audit"


Deze drie logs werden gegenereerd door een root commando uitvoering van tail -f /var/log/audit.log, gebruikmakend van de bovenstaande configuratie. 
Ze laten zien dat in de huidige werkdirectory (CWD) van /var/log/audit, de gebruiker root "tail -f audit.log" uitvoerde.

Een andere manier om logs te benaderen is door gebruik te maken van aureport. 
Aureport, uitgevoerd zonder parameters, genereert een uitvoer die het aantal gegenereerde events en hun verdeling weergeeft:

 user@debian:/var/log/audit$ sudo aureport


Samenvattend rapport
======================
Bereik van de tijd in de logs: 02/20/2022 01:17:01.139 - 03/13/2022 03:48:03.960
Geselecteerde tijd voor rapport: 02/20/2022 01:17:01 - 03/13/2022 03:48:03.960
Aantal wijzigingen in configuratie: 204
Aantal wijzigingen in accounts, groepen of rollen: 0
Aantal aanmeldingen: 9
Aantal mislukte aanmeldingen: 9
Aantal verificaties: 17
Aantal mislukte authenticaties: 3
Aantal gebruikers: 3
Aantal terminals: 9
Aantal hostnamen: 3
Aantal uitvoerbare bestanden: 142
Aantal commando's: 350
Aantal bestanden: 595
Aantal AVC's: 0
Aantal MAC-gebeurtenissen: 9
Aantal mislukte syscalls: 11422
Aantal anomaliegebeurtenissen: 0
Aantal reacties op anomaliegebeurtenissen: 0
Aantal crypto-gebeurtenissen: 0
Aantal integriteitsgebeurtenissen: 0
Aantal virt-events: 0
Aantal sleutels: 33
Aantal proces-ID's: 19394
Aantal gebeurtenissen: 42861


Je kunt dit rapport beperken tot een specifiek tijdsbestek door:

 aureport --start 03/01/2022 00:00:00 --eind 03/30/2022 00:00:00

Met ausearch kun je ook alle logs krijgen die te maken hebben met een specifieke gebruiker:

 ausearch -ua gebruikersnaam

Als je geïnteresseerd bent in iets dat een beetje meer parseerbaar is, wil je misschien kijken naar Slack's https://github.com/slackhq/go-audit[go-audit] alternatief voor de Auditd daemon, die logs in json uitvoert.

=== Verwerken van logs
Je zult waarschijnlijk de auditd logs direct van de machine af willen halen, om er zeker van te zijn dat ze niet gewijzigd kunnen worden door een aanvaller (zelfs als je de toegang tot audit trails traceert met de voorbeeld configuratie hierboven). Gebruik rsyslog, of een andere daemon die transmissie over TLS toestaat, of kijk naar het gebruik van audisp-remote, om te loggen naar een remote logging server.

Logs kunnen nuttig zijn voor een aantal redenen, waarvan er twee uitspringen:

Het eenvoudigweg wegschrijven van logs naar een onveranderlijke log store resulteert in informatie die gebruikt kan worden door een forensisch of incident response team dat reageert op een beveiligingsincident op uw systemen, dat op andere manieren is ontdekt;
    Het schrijven van logs naar een log store en SIEM die query's, waarschuwingen en andere mechanismen mogelijk maken, stelt uw organisatie in staat om effectief te reageren op beveiligingsincidenten.

Er is interessante informatie over hoe auditd logs te gebruiken in https://www.function1.com/2015/07/splunking-the-linux-audit-system[Splunk], en in https://www.graylog.org/post/back-to-basics-working-with-linux-audit-daemon-log-file[Graylog].

Bron: https://www.daemon.be/maarten/infosec.html[Information security]
